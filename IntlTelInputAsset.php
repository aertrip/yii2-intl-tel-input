<?php
namespace aertrip\intltelinput;
use yii\web\AssetBundle;
class IntlTelInputAsset extends AssetBundle {
	public $sourcePath = '@bower/intl-tel-input'; 

    public $css = [
    	// "build/css/intlTelInput.css",
    ];

    public $js = [
        "build/js/utils.js",
    	"build/js/intlTelInput-jquery.min.js"
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
